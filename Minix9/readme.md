![flow1.jpg](./flow1.jpg)
![flow2.jpg](./flow2.jpg)


**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

We were experimenting when we were doing the flowcharts to make sure we kept a balance between detail and keeping a broad picture so that the one watching can understand and follow along. We wanted to make it easy to understand and read and keep it simple but show enough so that it made sense. We were going a lot back and forward and some of the ideas came from old ones when we were brainstorming. The result we think have come out great as we kept the minimalist look without putting too much info and routings from one box to another on the flowchart. The conceptual itself was weighing more than the code also for this minix since we spent time discussing the possibilites with each other and got this down on paper first.


**What are the technical challenges facing the two ideas and how are you going to address these?**

We struggled with understanding each other vision and how it should be. Some of the technical issues will be as to how the code should function more precisely and we will address them by going over different solutions for the best way to optimize the code while still keeping the concept the same.


**In which ways are the individual and the group flowcharts you produced useful?**

The group flowchart was made to create a structure of the idea itself while on the other hand we saw that the individual one was more focused on the code itself and more directed towards that direction. They were both useful as they gave their own insights into what we could use and improve in their own ways. 

