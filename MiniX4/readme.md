![Minix 4 Screenshot.png](./Minix 4 Screenshot.png)

Link to run.me: [run.me](https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX4/index_for_minix4.html)

Link to code: [code](https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX4/sketch4.js)


**Description of work:**
In my MiniX4 you have to start by allowing the web cam to be turned on. After this you you receive the message: "Click to see if FBI is watching you". You therefor have to make a decision if you want to click the button or not. If you decide to click the button you will get three new texts which are: "Yes FBI is watching you", "Click again to see what they see" and also "Keep clicking on the button for new snapshots". If you follow through again and click there will be a snapshot taken and shown on the site. 

**Syntaxes I have used:**
I have used syntaxes such as "button = createButton" which makes a button that can be clicked. And added the following syntax: "button.mousePressed(change);" in order for it to be interactive. Furthermore there has been a use of "camcorder = createCapture(VIDEO);" to ensure that he video is enabled. This of course depends on whether or not the user has allowed and granted access for the camera to be used. I also used "image(camcorder, 200, 150, 500, 500);", this way the code is able to show you a snapshot of yourself whenever you click the button. And you are also able to click the button multiple times and get more snapshots taken without refreshing the page. However it is to be said that while I was programming and running the code my computer started using a lot of power to keep up. This may or may not result in a decrease in performance for some machines when trying the run.me . One can keep refreshing the site a couple times and try to let it load and or click on the button an extra couple times in order to make it work. The reason for this could be the use of the webcam that is turnt on the whole time which makes it possible to display multiple snapshots.

**Reasoning behind my choice for the MiniX4:**
I choose to make a code that gives the impression of how much control and access to data the FBI has. And if FBI can get access to this then other hackers will also be able to get access to your webcam. This is something that we know from Edward Snowden which was a whisteblower from the "National Security Agency" in America. Snowden has shown that we are not as protected in terms of having our own privacy as we may think. Audio and video is being captured and in most cases people are not aware when they are being spied on and by whom. These things are some important things to discuss in the modern world and find solutions that can decrease the likelihood of you being a victim of spying.

According to "Shoshana Zuboff on surveillance capitalism | VPRO Documentary" the data that our computer and phones collects can be sold for money to big companies. This is not something that normal people think about in their daily lives. The spy gadgets are made to be hidden so that the public does not suspect anything. This is how the companies can capture all our information because the information they do not yet have can probably be bought from another company. Data is very valuable and is being sold all the time. However there has been people like Snowden to expose this side of the digital era. This shows where our society is headed towards and we should try to educate us about this issue. 

