let camcorder;
let button;

function setup() {
  createCanvas(1500,1500);
  background(255,100);
}

function draw (){

button = createButton("Click to see if FBI is watching you");
button.position(710,220);
button.size(300,300);

//makes sure that the website is interactive and the button does something
button.mousePressed(change);




//enables video to be used
camcorder = createCapture(VIDEO);
camcorder.size(600,600);
camcorder.hide();

}

function change() {


//taking snapshot and showing when button is clicked again
camcorder.size(600,600);
//the size of the actual snapshot that is being shown
image(camcorder, 200, 150, 500, 500);
//text
fill("Black");
textSize(15);
textStyle(ITALIC);
text("Yes FBI is watching you",750,100);
text("Click again to see what they see",800,125);
text("Keep clicking on the button for new snapshots",850,150);

}
