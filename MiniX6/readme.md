![Uga snake game.png](./Uga snake game.png)


[Run.me](https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX6/index_for_minix6.html)

[Code](https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX6/sketch6.js)

[Code pt.2](https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX6/snake.js)



**Describe how does/do your game/game objects work?**
The game that I have made is heavily inspired by the snake game. When I was looking for youtube tutorials around making a game in p5 I stumbled upon this video from the coding train: [The Coding Train Video](https://www.youtube.com/watch?v=AaGK-fj-BAM) . Here it was explained what the different functions do and how to organize it. It works as the usual snake game. A twist of my own that I added was to change a couple things. Since I am interested in an nft project called "uga nation" I decided to take one of their nfts and replace to be the snake. I also changed the food that is spawning randomly to be hedera(hbar) which is a cryptocurrency that you can use to mint/buy this nft. Also I made sure that the actual tail was changed to the thing that the ape catched which in this case is the hedera(hbar) cryptocurrency. 


**Describe how you program the objects and their related attributes, and the methods in your game.**
I have used syntaxes such as pickLocation for the hedera and set it to be random. This means that each time the ape eats the hedera coin a new will spawn. And to make the ape move and have it focused as an object I also implemented the arrow keys on the right of the keyboard to be able to be used. Additionally the tail of the ape is set to also be hedera coins so that this way it is possible to have the ape look like the typical snake and have the typical snake-tail. 


**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**
> "Object-orientation names an approach to computing that views programs in terms of the interactions between programmatically defined objects (computational objects) rather than as an organized sequence of tasks incorporated in a strictly defined ordering of routines and subroutines." - - Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, "How To Be a Geek: Essays on the Culture of Software"

What this tells us is that there is focus on the object itself in these types of programs. This is different from having a program that executes a code in order. When the focus is on the object there are different things that can happen and by doing different things the order of code-execution will be different. This is seen whenever the snake hits a wall or goes into itself it will reset to 0 tale-length. However if one decide to continue catching Hedera(Hbar) the code will execute a different section to continuously keep randomly spawning new tokens on the screen.  



**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**
> It is often argued that with the development of object-oriented programming, a new era of interaction between humans and machines was made possible." - Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, "How To Be a Geek: Essays on the Culture of Software"

This interaction that Matthew Fuller is talking about reminds me of the interaction I had with my old nokia phone. I was a little kid back then and I had only two games on my phone. One of them so happened to be the notorious snake game. I think that this has shaped a part of childhood since it was one of the first games that I played. I believe that I find myself with many others in the same situation and that the snake game has transformed our culture and been a symbol for gaming. Nowadays we have more complex games however the snake game was an idea for many other games that are similar like slither.io I played back in around 7th grade. It is interesting to compare and see that it has tendencies from the original game however it is far more complex and advanced. For this evolution we can be thankful for the developers of the snake game which was the inspiration for games like this and many others and more to come in the future. 



I got inspiration from the coding train on youtube when I was looking for games. 
Something about nfts and uga nation. Catching hbar where they minted on recently which is a coin in itself but also a decentralized public network. 

