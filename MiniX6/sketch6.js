let s;
let scl = 55; //size of the squares (including ape size and hbar size)
let hedera; //the thing the ape is eating

function preload(){
ugaape=loadImage("ugaape.jpeg");
hbar=loadImage("hbar.png");
bg = loadImage("uga-background.jpeg"); //stating what the variable bg is
}

function setup() {


  createCanvas(windowWidth, windowHeight);
  s = new Snake ();
  frameRate(10);
  pickLocation();
}

function pickLocation() {
  let cols = floor(width / scl);
  let rows = floor(height / scl);
  hedera = createVector(floor(random(cols)), floor(random(rows)));
  hedera.mult(scl);
}

function mousePressed() {
  s.total++;
}

function draw() {
  background(bg);
  if (s.eat(hedera)) {
    pickLocation();
  }
  s.death();
  s.update();
  s.show();
  fill(255, 0, 100);
  image(hbar,hedera.x, hedera.y, scl, scl);
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    s.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    s.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    s.dir(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    s.dir(-1, 0);
  }
}
