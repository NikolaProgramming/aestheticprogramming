function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {


  background(100); //grey background


textSize(20)
textStyle(ITALIC)
  fill(250, 200, 10) //yellowish color for text
    text("Loading please wait",760,650);




  translate(850, 400); //position of the centrum for the line rotating
  rotate(90);

  let sc = second();

  strokeWeight(8);
  let secondAngle = map(sc, 1800, 50, 10, 500); //Map: Re-maps a number from one range to another.
//1800 and 500 decide how fast it rotates


  push();
  rotate(secondAngle); //makes the line rotate around
  stroke(250, 200, 10); //yellowish color for line
  line(200, 0, 100, 0);
  pop();

}
