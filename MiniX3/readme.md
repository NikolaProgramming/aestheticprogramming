![MiniX3 Screenshot.png](./MiniX3 Screenshot.png)


**MiniX3**

In my MiniX3 I decided to make a throbber that would rotate to give the feeling of a loading situation.

I found an interesting video on youtube where "The Coding Train" made a video about a clock. (https://www.youtube.com/watch?v=E4RyStef-gY)
I looked into the code and got inspiration for what I could do with my project. So I decided to make it look like it was moving each second to give the feel that there is happening something. "let sc = second();"

In my syntaxes I used stuff as Text and played around with that. The "loading" part. And I also did "let secondAngle = map" whereafter I put in "rotate(secondAngle);" which is to make the line rotate. As to how I constructed time in my throbber I used the following syntax: "let sc = second();". This was to start it of by declaring the variable. Later I used the "secondAngle = map" syntax where I put in my variable. I had just declared 1 second to be my sc variable. And that was what I used so that this way my throbber moves every single second. 

> "However, how the actual experience of time is shaped is strongly influenced by all sort of design decisions and implementations, both for humans and machines." - Hans Lammerant, “How humans and machines negotiate experience of time,”


This quote summarizes how certain aspects can feel longer than they actually are and other things can feel like they go by in an instant. This is because we use our natural sense of time/clock. And by this we form our experience of time in the exact moment. This can therefor be subjective and influenced by things such as design decisions. For my MiniX3 I decided to give it this clock type of line that is rotating. And it is not moving fast compared to other loading screens however it moves way faster than the typical clock-teller. Hopefully this can bring some quriosity to the user and make them stick around and watch the throbber without it being too uninteresting 


I have myself seen a throbber multiple times, which is a reassurance that the system has not crashed. A common place I have seen a lot of throbbers was when I was younger and used to play games on the web-browser. They have these simple games and most of them use "flash" to run I think. And they had often the typical loading screen which was mostly a bar that was filling up. So overall throbber gives a good experience to the user and reassures that it's a part of the process. A throbber can therefor be seen as a validator for a running and working device as well as a status for how far the machine has come loading certain stuff.




Link to run.me:
https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX3/index_for_minix3.html

Link to code:
https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX3/sketch3.js
