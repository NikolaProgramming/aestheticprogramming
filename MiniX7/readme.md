![Minix7 pt1.png](./Minix7 pt1.png)
![Minix7 pt2.png](./Minix7 pt2.png)


[Run.me](https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX7/index_for_minix7.html)

[Code](https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX7/sketch7.js)


**Minix7**

For this miniX7 I decided to revist my previous minix3 which was a throbber. I thought that I could add add a couple things to make the throbber more interesting. In this improved version of the throbber you can see a set of lines that apear around a circle. Both the inner circle and the outher lines can both be seen as throbers. Additionally I have a text saying "loading" so that it is obvious that the program is a loading screen. The big difference between the improved and the previous version is that this gives more to look at. Firstly you have the circle which is the attention-grabber in the start. This is the combined with the outher improved version. On the outer lines it leaves a track of the lines already created. This way you can see if you have been long at the loading screen. Additionally in the inner circle there is a small overlap that gives a cool effect since it is in fact two throbbers running at the same time. The focus is switched though from the inner throbber to the outer throbber after the inner throbber fills out its own circle. 

> We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities. - Aesthetic-Programming, Preface, Winnie Soon & Geoff Cox

This quote can be used to understand how we as humans are living in the modern world with the digital things around us. A big factor that played in our digitalization was the programming aspect. For a phone to work it needs programming and codes that are executable when you open a program. Furthermore the phone needs to have a pleasant design and aesthetic. Both on the outside and inside. It has to be easy to execute tasks and this can be done by organizing things with the aesthetic side of programming. A phone also typically has a starting screen when it is turned on. This is to ensure the consumer that there is something happening and in this way giving confirmation. There are also throbbers specifically in some apps which let the user know that the phone is still working and that the screen is not just froozen. Aesthetic programming has become a very big part of our lives because of how much the digital aspect fills in our lives. Therfor we need the aesthetic aspect in order to be able to navigate and use very advanced units without knowing the code itself. In other words you can say aesthetic programming is here to present to the user in a digestable way. This is exactly what I have done with my throbber by letting the user know that the program is working with motion.
