**Which sample code have you chosen and why?**

We have chosen the SketchRNN sample code, because we found it very compelling. We explored the code and the programme, and found (after quite a while playing with it) that we could change the objects that are drawn. That gave us a laugh, and we were amazed at how well some of them were drawn, when others weren’t as well executed.

**Have you changed anything in order to understand the program? If yes, what are they?**

Trying to decode the sample code wasn't easy at first, but little by little, we started to piece the code together. We tried deleting parts of the code, to see how it affected the programm. And it gave clarity as we saw what elements were missing to complete it.

**Which line(s) of code is particularly interesting to your group, and why?**

We discovered the line: “model = ml5.sketchRNN('cat', modelReady);” at line 32, which made us curious. Above the line the creator/creators link to a repository of “model.js” which are

**What are the syntaxes/functions that you don't know before? What are they and what have you learnt?**

A function that we didn’t know before was the “function gotSketch(err, result) {“. What this function is there for essentially is merely for the program to do something with the result generated from the previous lines which were there to create the model.

**How do you see a border relation between the code that you are tinkering with and the machine learning applications in the world (e.g creative AI/ voice assistants/driving cars, bot assistants, facial/object recognition, etc)?**

As we play with the code and change attributes and values one may start to find out how customized and personalized you can make a project with code. One site that really stand out here is the site where random faces are being generated. Here is an example of an already existing database that is being used by the AI to create a fake person. This is done by changing attributes to the object (the face in this case).

**What do you want to know more/ What questions can you ask further?**

As we can see objects can be changed with code and this makes room for new possibilities. A question that one can ask is how far one really can take the objects. How much can they be changed and will it come to the point where we will have AI made videos about politicians saying things that the real person would not have said? These are all serious topics and something to think about from the possibilities that one can do with simply changing the object and AI.
