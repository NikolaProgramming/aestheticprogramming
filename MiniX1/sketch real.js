//green man is cool

function setup() {
  // put setup code here
	createCanvas(800,800);
  background("white");
}

function draw() {
//color on boarder of ellipse
  stroke('#FFD1F8');  //the colour
strokeWeight(0.8); // how thick the line is
fill("#D5FFE6"); //fill inside ellipse
ellipse(470,350,300);


//Eyes and mouth
  stroke ('#73FA78'); //green border colour
  push()
  	strokeWeight(2); //thick

  //mouth
    fill("white");
  ellipse(460,440,90,70);
  push() //start for isolating the rotate code for only one ellipse
    rotate(0.2 / 2.0); //rotate the vertical circle layer


        fill("black");
    ellipse(420,300,70,180);
    fill("white") //eyefill
      ellipse(420,300,30,180);

      fill("black");
    ellipse(420,300,30,30);

  pop() //end of isolation for rotating ellipse

    fill("black");
  ellipse(500,300,70,180);

fill("white") //eyefill
  ellipse(500,300,30,180);

fill("black");
  ellipse(500,300,30,30);

}
