![MiniX 1 Screenshot.png](./MiniX 1 Screenshot.png)

This is my first readme and my first miniX. I had a hard time getting started so I decided to get started by doing something simple. So I went to openprocessing.org and found this sketch: https://openprocessing.org/sketch/1426391

I really liked the simplicity and artistic style behind it so this was my inspiration to get started. I looked at the code for the sketch and here I found different commands which was the starting point for my design. I went ahead and created an ellipse and then another one and a couple more afterwards. I used this command here: https://p5js.org/reference/#/p5/ellipse . Later I decided to play around with the ellipses and give them a different character. I could achieve this by using using the stroke and strokeweight codes. Stroke is the colour of the borderline while the strokeweight is how thich the borderline will be. The codes: https://p5js.org/reference/#/p5/stroke and https://p5js.org/reference/#/p5/strokeWeight . Here I choose to make the strokeweight on the eyes bigger to make them become more of a type of center and focus. 

Now that I had my ellipses and had choosen the borderline colour and the shape it was time to select the colors to fill out specific places. For this I used: https://p5js.org/reference/#/p5/fill

I also decided to give the eyes an asymmetric type of placement to give it more depth, and for this I used: https://p5js.org/reference/#/p5/rotate
I had to make sure however that I only rotated the one eye (left eye) and therefor I used push and pop which is always used together. 
https://p5js.org/reference/#/p5/push
https://p5js.org/reference/#/p5/pop


As I went through the coding process the look of my design changed a lot and I had different looks of my design but in the end it ended up as a green face with some special eyes. 

Overall the experience was a little hard at first but now that I have tried the simple stuff I feel more confident in that and can do it faster now. This makes room for new codes and functions to be learned and used because I do not have to focus about the completely basic stuff anymore.

Some of the things I went through and made them fit on the screen by trying different combinations of numbers and this will hopefully get easier by time to distinguish wich values are for what. 

From the video I see the difference between typical Java and P5. P5 seems to be the lighter version and that it is easier with this language to project things onto the screen. P5 seems to be a better way to get into programming based on this. This opinion is based on "LEARNING WHILE MAKING P5 JS Lauren McCarthy on youtube". Another thing that is mentioned here is to not be afraid to just start without a plan. It should be okay to take a risk and try different things.



Link to design: https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX1/index%20for%20minix1.html

Link to code: https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX1/sketch%20real.js
