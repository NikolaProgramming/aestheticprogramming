![MiniX 2 Screenshot pt.1.png](./MiniX 2 Screenshot pt.1.png)
![MiniX 2 Screenshot pt. 2.png](./MiniX 2 Screenshot pt. 2.png)


**Description of the program**

The program is about a smiley that shows whenever you choose to click and hold the mouse button. The major thing that is making this possible is the syntax if and else. This syntax makes it possible for the emoji to change. It changes from a black hole to a happy smiley and the syntax mouseIsPressed is responsible for making the previous if-else syntax work with the click of a mouse button. 


It was made mainly with the function "if (mouseIsPressed === true)" and then "else" if it's not pressed. Other than this there was also used "ellipse" and "fill" I experimented with. Also I used "arc" to make the emoji and make a mouth onto it. 

The main message of this is to give people a good day by surprising them with a sudden emoji once they click the screen. It is a little surprise that can help lift the mood if needed and if not it can be a cool thing you would be able to send around to your friends and give them a surprise. And the message on the screen is "positive vibes" which can also be a support for the happiness to take effect.

The thing I liked the most about this minix2 is that I managed to make my design more "active" instead of static in a way. My previous design was purely static so this was a big improvement to be able to make something that had some interaction with the user.


**Social/Cultural context**


> The process of implementing emoji modifiers stages race, gender and technologies in a way that seems exemplary of how identity politics is being transformed from a cultural issue into a technical challenge and eventually into a commercial asset." - Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, “Modifying the Universal,” in Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, eds.

This quote sums up why the yellow color was choosen to be the face color. Just as we have the thumbs up emoji in different colors once u hold it then this is as well an attemt to not choose white or black color. Therefor I choose yellow. Now if I should have been ultra aware about the color I should have either made more emojis in the same place or make the user choose the start what color he/she wanted the face to be. This is a socail/cultural topic and can be sensitive to some since racism still is to be found in this world. If I should criticize my design it would probably this which could have been prevented by creating a starting page and then later going onto the actual main page. However this would require some serious knowledge of how to link it all together. However the idea is there as a prevention for this issue. I tried being as neutral as we are used to smileys being yellow which is a social/cultural standard.


See my design: https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX2/index_for_minix2.html

The code: https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX2/sketch2.js
