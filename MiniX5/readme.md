[Run.me](https://nikolaprogramming.gitlab.io/aestheticprogramming/MiniX5/index_for_minix5.html)

[Code](https://gitlab.com/NikolaProgramming/aestheticprogramming/-/blob/main/MiniX5/sketch5.js)

![MiniX5 Screenshot.png](./MiniX5 Screenshot.png)

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**
In my MiniX5 I have different rules. I have let my "x" and "y" set to 0. This makes sure that it is the starting position in the top left of the windowframe.
Additionally I have introduced "space" which is set to the value 20. This is later used to in the syntax to make the place where the objects are drawn change position. The end result of the program is to create a randomized art that is extremely hard to replicate but not entirely impossible. However because it is so hard to recreate something it means that each time the code is run the result will be unique. 



**What role do rules and processes have in your work?**
The rules in my MiniX5 make sure that there is a order in the art. Even tho the art is randomly generated there are specific rules implemented to make sure that it stays within these limits. For instance I have made it so that the lines that are generated fill up from left to right and afterwards jump down to the next line. For this an if statement has been used. Additionally the circles are programmed to fill up from the top to the bottom and from this be able to fill up eventually a big part of the canvas. I have used another if-statement combined with "random" syntax to make it so that there will be either a circle or line drawn. The chance for a line to be drawn is much more unlikely than a circle and this results in more spaceroom between each line compared to the circles.


**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**
> "A random process generates a sequence of values selected from a set of possible values according to a probability distribution."  (Nick Montfort et al. “Randomness,” 10 Print) 

This quote sums up how how I view auto-generation. You are letting the code run and select random numbers. These numbers are selected based on the specific possible values that are able to be auto-generated. Furthermore there is tthe possibility to add a form of weight so that one outcome is more favored and more likely to suceed. This is able to make one realize how this can be used to express certain visual things while maintain our own borders in terms of what the code is able to auto-generate. It is interesting to experiment with it since you have the control and can "manipulate" the program to be favoring a result you would like to happen more often. In this modern world things like this auto-generation can be seen in the nft world. Here the developers select some attributes and when you mint an nft you will in most cases get a randomized picture that is within the limits of what attributes are available for the specific collection. Here you can typically find that attributes that are more rare, meaning less likely to be minted also are the nfts in the collection that are more valuable to the buyers. There is this unwritten agreement that attributes which are less likely to be minted also are the ones that make it more rewarding to have one because it is a rare thing to happen. 



