let space = 20;
let x = 0;
let y = 0;

function setup(){
  createCanvas(windowWidth, windowHeight);
  background(1000); //white background
}

function draw(){
strokeWeight(2); // Thickness of line
stroke(100); // Sets the color used to draw lines and borders around shapes.
  if(random(5) < 0.5){ //if number is less than 0.5 a line will be drawn.
      line(x,y+space,x+space,y);
  } else {
      circle(y,x,+space,y+space); //if random number is from 0.5 to 5 a circle will be drawn.
  }

x =x+space; //every frame the drawing place moves with "space" distance

  if(x > width){ ///makes the line and circle start from a new line each time it goes beyond the windowwidth
    x = 0       ////makes the line and circle start from a new line each time it goes beyond the windowwidth
    y =y+space; //how big the space is between the lines on the vertical horizon
  }

}
